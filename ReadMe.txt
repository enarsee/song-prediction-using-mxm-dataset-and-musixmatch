Group ID: Project_Report_Group

Programming Language: Python 2.7
Library Dependencies: Sci-kitlearn, pytables, sqlite3, ConfigParser, nltk, enchant

Steps:

- Data Fetching
  1) Download the MSD subset from (labrosa.ee.columbia.edu/millionsong/)
  2) Set the directory path and files to fetch in config.ini
  3) Run the file "writeData.py" and obtain a dataset.sqlite file
  4) Set number of top words in config.ini
  5) Run topwordsParser.py to obtain a topwords.sqlite file

  1) Run tableclean.py to remove outliers

- Data Cleaning & Integration
  1) Open both SQLite files, topwords and dataset
  2) Set proper data types for columns ( Change to float )
  3) Create a new view by joining both of them
  4) Run tableclean.py to remove outliers

 - Clustering
 1) Set the correct sqlite file link and no of clusters in config.ini
 2) Run Main.py and wait for clustering output
 3) The dataset sqlite file will contain the updated labels and predictions
