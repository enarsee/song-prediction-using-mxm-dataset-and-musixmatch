import sqlite3 as lite
import math
from config import *
 
#This class contains all functions to read or write to sqlite dataset
class sqlitefetch():
    def __init__(self):
        self.config = config()
        #CHANGE TO YOUR LOCAL CONFIGURATION
        self.con = lite.connect(self.config.getConfig("Database")["SQLITE"])
        self.con.row_factory = lite.Row
 
    #Fetch all rows
    def fetchALL(self):
    	attribute_list= config.getConfig('Fetching')['Attributes'].split(",")
    	
    	string=','.join(attribute_list)
        q= ("SELECT " + string + " FROM "+self.config.getConfig("Tables")['dataset'])
        
        res = self.con.execute(q)
        result = res.fetchall()
        return result
 
    #Each row represents a dictionary, the dataset is a list
    def createDict(self):
        result=self.fetchALL()
        final_list=[]
        for row in result:
        	#Append instance to sqlite
        	final_list.append(dict(row))
        return final_list
 
    #Write clustering label and training model predictions to sqlite
    def writeLabel(self,ColumnName,label):
        print("Labels assigned to "+str(len(label))+" objects")
        for i in range(0,len(label)):
            q="UPDATE dataset SET "+ColumnName+"="+str(label[i])+" WHERE rowid="+str(i)
            self.con.execute(q)
            self.con.commit()
 
    #Write column to sqlite
    def write(self,file_data):
        cur = self.con.cursor()
        
        for file in file_data:

            attr = "("
            for item in self.attribute_list:
                attr = attr+item+", "
            attr = attr[:len(attr)-2]+")"


            insertString = "(null,null,null,null,"
            param = []
            
            for item in self.attribute_list:
                param.append(str(file[str(item)]))
                insertString = insertString+"?, "               
            insertString = insertString[:len(insertString)-2]+")"
            
            q = "INSERT INTO main.dataset VALUES"+insertString
            try:
                cur.execute(q,tuple(param))
            except:
                continue
            
        self.con.commit()
        print 'Inserted'

    #Create a column of row ids
    def initialize(self):
        cur = self.con.cursor()
        q = "DROP TABLE IF EXISTS dataset"
        cur.execute(q)

        insertString = "("+self.config.getConfig("Fetching")["Additional"]+ ", "
        for item in self.attribute_list:
            insertString = insertString+item+", "
        insertString = insertString[:len(insertString)-2]+")"

        q = "CREATE TABLE dataset "+insertString
        cur.execute(q)

        self.con.commit()