#Clean Database of Null Errors
import sqlite3
from config import *

c = config.config()
conn = sqlite3.connect(c.getConfig("Database")['dataset'])
curr = conn.cursor()

tables = ["dataset","topwords"]

for table in tables:
	q = "PRAGMA table_info("+table+");"
	print "Cleaning Table "+table
	res = curr.execute(q)
	columns = res.fetchall()
	for column in columns:
		
		q = "UPDATE "+table+" SET "+column[1]+" = 0 WHERE "+column[1]+" is null;"
		curr.execute(q)
		q = "UPDATE "+table+" SET "+column[1]+" = 0 WHERE "+column[1]+" = 'nan';"
		curr.execute(q)
		
conn.commit()



