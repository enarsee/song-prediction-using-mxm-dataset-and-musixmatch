import os
import sys
import time
import glob
import datetime
import sqlite3
import numpy as np
import train
from dbCheck import *
import hdf5_getters as GETTERS
import time
from config import *
from sqliteFetch import sqlitefetch
  
class fetchData():
  
        def __init__(self,attribute_list):
                config = config()
                # CHANGE IT TO YOUR LOCAL CONFIGURATION
                self.msd_subset_path=config.getConfig("Main")['Dataset']
                self.max = int(config.getConfig("Fetching")['MaxFiles'])
                self.interval = int(config.getConfig("Fetching")['FileInterval'])
                self.msd_subset_data_path=os.path.join(self.msd_subset_path,'data')
                self.msd_subset_addf_path=os.path.join(self.msd_subset_path,'AdditionalFiles')
                self.attribute_list = attribute_list
                self.topwords = dbCheck()
                self.obj_write=sqlitefetch(self.attribute_list)

          
        # function to iterate the files
        def apply_to_all_files(self,maxFiles,funcName,func=lambda x: x,ext='.h5'):
            self.obj_write.initialize()
            file_data=[]
            """
            From a base directory, go through all subdirectories,
            find all files with the given extension, apply the
            given function 'func' to all of them.
            If no 'func' is passed, we do nothing except counting.
            INPUT
               func     - function to apply to all filenames
               funcName - name of function to call from h5 GETTERS
               ext      - extension, .h5 by default
            RETURN
               number of files
            """
            cnt = 0
            count = 0
            # iterate over all files in all subdirectories
            print "Fetching Files.."
            for root, dirs, files in os.walk(self.msd_subset_data_path):

                files = glob.glob(os.path.join(root,'*'+ext))
                #print files
                # count files
                # apply function to all files
                if(count>self.max):
                        return
                for f in files :
                    track_id = (f.split("/")[len(f.split("/"))-1]).split(".")[0]
                    
                    if(not(self.topwords.check(track_id))):
                        continue 

                    cnt +=1
                    count += 1
                  
                    dataItem=func(funcName,f) 
                    

                    #Apend data to the list
                    file_data.append( dataItem )
                    if(cnt==self.interval):
                        self.obj_write.write(file_data)
                        del file_data[:]
                        file_data = []
                        cnt = 0
                    
                #print cnt
            #print "Files",cnt
                
            #print list_data
            return file_data

       

  
        def func_to_get(self,funcName,filename):     
            # open the h5 song file to read it
            h5 = GETTERS.open_h5_file_read(filename)
  
            #Get data item according to functionName
            
            data_list = dict()
            #Get all attributes and append to list

           

            for attribute in self.attribute_list:
                data_list[attribute] = eval("GETTERS.get_"+attribute+"(h5)")

            #Close the file
            h5.close()
            return data_list