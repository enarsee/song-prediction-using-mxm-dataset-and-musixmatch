'''
Created on 29 Oct, 2014

@author: Akriti
'''


from sklearn.cluster import KMeans
from sklearn.cluster import k_means
import sklearn.cluster
from sklearn.preprocessing import scale
import numpy as np
from sklearn.feature_extraction import DictVectorizer

def kmeansCluster(final_matrix, number_of_clusters):
    return k_means(final_matrix, number_of_clusters)
    

