'''
Created on 7 Nov, 2014

@author: Akriti
'''


def print_calculations(targets, result, number_of_clusters):
    counts={}
    accuracy_count=0
    for n in range(number_of_clusters):
        counts[n]={"TP":0, "FP":0, "TN":0, "FN":0}  
    for i, label in enumerate(targets):
        if label == result[i]:
            accuracy_count+=1
            counts[label]["TP"]+=1
            for num in range(number_of_clusters):
                if num!=label:
                    counts[num]["TN"]+=1
        else:
            counts[label]["FN"]+=1
            for num in range(number_of_clusters):
                if num!=label:
                    if result[i]==num:
                        counts[num]["FP"]+=1
                    else:
                        counts[num]["TN"]+=1
                        
    avg_precision, avg_recall, avg_f =0,0,0
    count_precision=0
    count_recall=0
    count_f=0
    for n in range(number_of_clusters):   
        print "Results for cluster : " ,n, " :"
        TP, FN, TN, FP= counts[n]["TP"],  counts[n]["FN"], counts[n]["TN"],  counts[n]["FP"]
        print "True Positives : ", TP
        print "False Negatives : ", FN
        print "True Negatives  : ", TN
        print "False Positives : ",FP
        
        """If the denominators in question end up being zero, then the measures in the question are NA. Hence, such 
        cases are not considered while calculating averages. """
        
        if TP + FP ==0:
            print "The Prediction Engine did not assign any instance to this label. Hence precision is NA."
        else:    
            precision=TP/(TP+float(FP))
            count_precision+=1
            avg_precision+=precision
            print "Precision/Positive Predictive Value(PPV) : ", precision
            
        if TP + FN ==0:
            print "The Clustering algorithm did not assign any instance to this label. Hence recall is NA."
        else:
            recall=TP/(TP+float(FN))
            count_recall+=1
            avg_recall+=recall
            print "Recall/Sensitivity/True Positive Rate(TPR) : ", recall
            
        if TP + FP + FN==0:
            print "F-measure not applicable."
        else:
            f_measure= 2*TP/(float(2*TP)+FP+FN)
            count_f+=1
            avg_f+=f_measure
            print "FMeasure :", f_measure
              
        print "======================================="
        
    print"----------------------------------------------------------------------"    
    print "OVERALL METRICS :"
    print "Average Precision : ", avg_precision/count_precision
    print "Average Recall : ", avg_recall/count_recall
    print "Average F-measure : ", avg_f/count_f
    print "Overall Accuracy : ", float(accuracy_count)/len(targets) 
