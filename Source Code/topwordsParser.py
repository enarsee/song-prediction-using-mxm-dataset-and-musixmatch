#Top Words Stemmer and Parser
#Author : Nishant
#Last Updated : 6th November

import sqlite3
import nltk
import csv
from nltk.corpus import stopwords
import enchant
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.stem import PorterStemmer
from config import *
import sys

config = config()
#Connect to an sqlite Database
conn = sqlite3.connect(config.getConfig("Database")["MXM"])
cursor = conn.cursor()
noOfWords = 10

#Get Stopwords
stopList = stopwords.words('english')
stopList = stopList + stopwords.words('french')
count = 0
wordList = list()
originalList = list()
lmtzr = WordNetLemmatizer()
stemmer=PorterStemmer()

#Get Dictionary
english = enchant.Dict("en_US")
french = enchant.Dict("en_US") #To be Installed

#Run Through our CSV
with open("topwords-train.csv","rb") as csvFile:
	c = csv.reader(csvFile,delimiter=' ', quotechar='|')
	print "Removing Stopwords.."
	for row in c:
		originalList.append(row[0])
		if(not(row[0] in stopList) and (english.check(row[0]) or french.check(row[0]))):
			wordList.append((row[0]))

wordList = set(wordList)
wordList = list(wordList)
print "Total Words : ",len(wordList)

shortenedList = wordList[:noOfWords]
shortenedListIndex = list()


#Create a table for the words
insertString = ""
for word in shortenedList:
	shortenedListIndex.append(originalList.index(word)+1)
	insertString = insertString + word + ", "
insertString = insertString[:len(insertString)-2]
print insertString
print shortenedListIndex

cursor.execute("DROP TABLE IF EXISTS topwords")
cursor.execute("CREATE TABLE topwords (track_id, "+insertString+")")

#Read main file
with open("mxm_dataset_test.csv","rb") as csvFile:
	c = csv.reader(csvFile,delimiter=',', quotechar="'")
	print "Reading Tracks.."
	count = 0
	for row in c:
		#print row
		if(count>40000):
			break
		t = (row[0],)

		cursor.execute("INSERT INTO topwords(track_id) VALUES(?)",t)
		for i in range(1,len(row)):

			result = row[i].split(':')
			#print result
			if(int(result[0]) in shortenedListIndex):
				word = shortenedList[shortenedListIndex.index(int(result[0]))]
				t = (row[0], )
				cursor.execute("SELECT COUNT(*) FROM topwords WHERE track_id = ?",t)
				(number_of_rows,)=cursor.fetchone()

				t = (result[1],row[0])
				cursor.execute("UPDATE topwords SET "+word+"=? WHERE track_id = ?",t)
		count += 1					
				

		
conn.commit()
				#print row[0],word,result[1]




