'''
Created on 25 Oct, 2014

@author: Akriti
'''

import numpy
import scipy
from sklearn.feature_extraction import DictVectorizer
from sklearn.naive_bayes import GaussianNB
import math
from sklearn import svm
from sqliteFetch import sqlitefetch
from analyse import print_calculations


def test_gnb(final_matrix, targets, number_of_clusters):
    
    gnb=GaussianNB()
    #print len(final_matrix)
    split_index= int(math.ceil(len(final_matrix)*0.60))
    #print split_index
    result=gnb.fit(final_matrix[0:split_index], targets[0:split_index]).predict(final_matrix[split_index:])
    #To print the predicted labels
    #print y_pred_nb
    
    Data_obj=sqlitefetch()
    Data_obj.writeLabel("PredictionsGNB",result)

    #print result
    print "Predictions added to database. Calculation results returned by SVM :"
    print_calculations(targets[split_index:], result, number_of_clusters)
    

def test_svm(final_matrix, targets, number_of_clusters):
    
    
    split_index= int(math.ceil(len(final_matrix)*0.60))
 
    """This commented code segment can be used to implement rbf kernel in svm. In fact, if "rfb" is replaced
    by "linear" then that should implement linear kernel. The gamma value is a parameter whose value I just
    randomly played around with - don't know what it means..."""
    
    svm_model = 'rbf'
    y_pred_svm= svm.SVC(kernel=svm_model, gamma=0.234)
    y_pred_svm.fit(final_matrix[:split_index], targets[0:split_index])
    resultRadial=y_pred_svm.predict(final_matrix[split_index:])
    
    Data_obj=sqlitefetch()
    Data_obj.writeLabel("PredictionsSvmRadial",resultRadial)

    #print result
    print "Predictions added to database. Calculation results returned by SVM - radial :"
    print_calculations(targets[split_index:], resultRadial, number_of_clusters)
    
    svm_model1 = svm.LinearSVC(C=1.0)
    svm_model1.fit(final_matrix[:split_index], targets[0:split_index])
    resultLinearSVC=svm_model1.predict(final_matrix[split_index:])
 
    Data_obj=sqlitefetch()
    Data_obj.writeLabel("PredictionsSvmLinearSVC",resultLinearSVC)

    #print result
    print "Predictions added to database. Calculation results returned by SVM - Linear SVC :"
    print_calculations(targets[split_index:], resultLinearSVC, number_of_clusters)
    
    