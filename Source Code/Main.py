'''
Created on 2 Nov, 2014
  
@author: Akriti
'''
  

from cluster import kmeansCluster
from train import test_svm
from train import test_gnb
from sklearn.feature_extraction import DictVectorizer
from sqliteFetch import sqlitefetch
import time
import datetime
import numpy
 
"""
WHY WE NEED DICT VECTORIZER
Simply because that gives the matrix format that is needed by various scikit modules to implement
learning models, clustering, feature selection etc etc
The final_list that will be made later will also be the way it is because that is the kind of input 
dict vectorizer takes, since it is a VECTORIZER of a list of DICTs.
 
"""

def makeDictVectorizer(final_list):
    vec = DictVectorizer()   
    final_matrix= vec.fit_transform(final_list).toarray()
    return final_matrix
 
def strtimedelta(starttime,stoptime):
    return str(datetime.timedelta(seconds=stoptime-starttime))
         
  
if __name__=="__main__":
     
    Data_obj=sqlitefetch()
    final_list=Data_obj.createDict()
    c = config()
 
    #Fetching dataset in the form of a list
    #Each row is represented as a dictionary
    #final_list is a list of dictionaries  
    print "performing clustering"
     
    # We need to bring the final_list into a suitable matrix format first
    t1 = time.time()
    final_matrix = makeDictVectorizer(final_list)  
    
    """CAN SET NUMBER OF CLUSTERS HERE"""
           
    number_of_clusters=int(c.getConfig("Clustering")['clusterNumber'])
    
    centroid, label, inertia = kmeansCluster(final_matrix, number_of_clusters)
    t2 = time.time()
    print 'Clustering Performed in :',strtimedelta(t1,t2)
                 
    print "Centroid-",centroid
    print "label",label
    print "inertia",inertia
 
    """What is most important to us at the moment is the label array, which contains a list of labels. 
    label[i] represents the label(the cluster number - such as 0, 1, 2, 3, 4) for the ith song. 
     
    """
    
    """TEST PRINTS"""
    cluster_division=numpy.zeros(number_of_clusters)
    
    print "--------------LABELS--------------------"
     
    for l in label:
        cluster_division[l]+=1
    
    print "CLUSTERS DIVISION: "
    for entry in cluster_division: 
        print entry, "---",
     
    """
    Now we implement the learning model - where svm/naive bayes tries to learn the labels according to the clustering results
    and predicts the labels for a test set
    """
    
    t1=time.time()
    
    test_svm(final_matrix, label, number_of_clusters)
    
    t2=time.time()
    
    print 'Training Performed by SVM in :',strtimedelta(t1,t2)
    
    t3=time.time()
    
    test_gnb(final_matrix, label, number_of_clusters) 
    
    t4=time.time()
    
    print 'Training Performed by Naive Bayes in :',strtimedelta(t3,t4)
      
     
    #print centroid
    #print label
    #print inertia
     
    #Write Labels generated from kmeans clustering to sqlite File
    Data_obj.writeLabel("Labels1",label)
 
    
     
    