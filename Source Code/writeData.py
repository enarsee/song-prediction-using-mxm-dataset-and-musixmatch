from fetch import fetchData
from sqliteFetch import sqlitefetch
from config import *
import math

if __name__=="__main__":
    #Creating class object
    
    config = config()

    maxFiles = int(config.getConfig('Fetching')['MaxFiles'])
    #List of attributes to fetch from song files
    attribute_list= config.getConfig('Fetching')['Attributes'].split(",")
    
    obj_fetch=fetchData(attribute_list)

    print "Constructing attribute_list"
    #Dictionary where the key is an attribute and value is a list
    data_dictionary={}
    #obj1.initialize()
    
    attribute = ""
    #Populating the data dictionary
    #for attribute in attribute_list:
        #Iterating through all files to fetch the attribute information
        #print "Appending items for.." + attribute

    file_data = obj_fetch.apply_to_all_files(maxFiles,"get_"+attribute ,func=obj_fetch.func_to_get)
    print "Now writing !"
        #print data_dictionary[attribute]
        #Write to sqlite file
    
    #obj_write.write(file_data)
    